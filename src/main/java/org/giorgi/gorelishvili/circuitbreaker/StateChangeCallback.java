package org.giorgi.gorelishvili.circuitbreaker;

public interface StateChangeCallback {

	void onStateChange(CircuitBreakerState updatedState);
}
