package org.giorgi.gorelishvili.circuitbreaker;

public enum CircuitBreakerState {
	OPEN,
	HALF_OPEN,
	CLOSED
}
