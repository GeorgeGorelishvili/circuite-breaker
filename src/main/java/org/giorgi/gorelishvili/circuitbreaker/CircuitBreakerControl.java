package org.giorgi.gorelishvili.circuitbreaker;

public interface CircuitBreakerControl {

	boolean canCall();

	void endCall();
}